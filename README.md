# TCK Specification

When adding new TCK(s) in a MR to this repository, a CI test with the following stages is triggered:
```yaml
stages:
  - extract-variables # Pre-Validation
  - checks # Validation
  - generate # Actual TCK Generation
  - compare # Comparison between Configurations
  - test # Test new TCKs
  - on-merge # on `main`: pushes TCKs/tags/encoding keys for use
  - deploy # cvmfs deployment of `/cvmfs/lhcb.cern.ch/lib/lhcb/tcks.git`
```
The CI test performs several important validation checks, generates the TCKs, compares to the previous TCKs of the same type, and tests the performance of the new TCKs. 
If interested, see: [pipeline definition](https://gitlab.cern.ch/lhcb/tck-specs/-/blob/main/.gitlab-ci.yml?ref_type=heads) or a recent pipeline for more information.

Upon merging new TCK specifications, the TCKs will be pushed to https://gitlab.cern.ch/lhcb/tcks, any new metadata is pushed to https://gitlab.cern.ch/lhcb-conddb/file-content-metadata and if any were `commit-based` TCKs, the appropriate commits in the stack project will be tagged and pushed.
The metadata and tcks repositories are then deployed on `cvmfs` under `/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/file-content-metadata.git` and `/cvmfs/lhcb.cern.ch/lib/lhcb/tcks.git` respectively.

A brief overview was given at the 111th LHCb Week's Computing and Software Parallel session https://indico.cern.ch/event/1378705/#13-tck-infrastructure. More description can be found at 

## Examples
Currently able to generate "stack-based" and "commit-based" TCKs for Hlt1.
Creating multiple TCKs at once is supported.

"stack-based" TCKs use the cvmfs-mounted release versions and `lb-run Allen/vXrYpZ` to generate the tck. 

```yaml
TCK: 0x1000100a
workflow: "stack-based"
parameters:
    process: "Hlt1"
    type: "hlt1_pp_default"
    label: "Hlt1 default, <describe what changed w.r.t. previous TCK>"
    stack: "RTA/2023.08.04" # As defined in https://gitlab.cern.ch/lhcb-core/lhcbstacks/
    settings: "hlt1_pp_forward_then_matching_no_ut_no_gec"
```

"commit-based" TCKs allow for modifications to released versions of each stack project. Any Allen-dependencies not listed in the `commits` dictionary will default to using the cvmfs-mounted release versions to spare time when building Allen s.t. `Allen/run` can be used to generate the tck.
__NB:__ These commits can only differ from released versions by python files, if not a new release needs to be organised.
```yaml
TCK: 0x1000102a
workflow: "commit-based"
parameters:
    process: "Hlt1"
    type: "hlt1_pp_default"
    label: "Demo: Added prescale to single muon highpt"
    stack: "RTA/2024.01.26"
    settings: "hlt1_pp_forward_then_matching_no_ut"
    commits:
        Allen: '79f0994e26036b378e368f3c8d7dfb13a08309b8'
```


## Deleting TCK tags
Before deleting a TCK tag it is very important to first check with someone important. Do not take lightly!

## Support
@lugrazet, @miolocco, @rmatev

## Authors and acknowledgment
@lugrazet, @miolocco, __Supervisor:@rmatev__