For use in Emergencies.
Specifically when: 
  A new TCK is necessary 
  The automated pipeline is not working
  The TCK Infrastructure experts (@lugrazet, @miolocco, @rmatev) cannot be reached/ cannot fix the issue in an appropriate time period.

To run the testbench to evaluate TCKs, it is necessary to be run these instructions on a GPU machine, e.g. `ssh hlt_oper@n4050501`. Thus you may find it easier to use such a machine throughout the process.

## Disambiguation, 'stack-based' vs 'commit-based' TCKs
We create a new TCK in two scenarios: a stack release or Python patches ("hotfixes") above a stack release.
 - stack-based directly uses release versions mounted on CVMFS.
 - commit-based in contrast allows per-Project python-only hotfixes above of a released version.

Generating 'commit-based' TCKs are the more likely version to break/ need urgent local generation so these instructions focus on that schema.
If you wish to create a 'stack-based' TCK, you need a stack from https://gitlab.cern.ch/lhcb-core/lhcbstacks/-/tree/master/data/stacks/RTA and then replace `Allen/run` with `lb-run Allen/<allen_version>`. Also you can ignore the _Reproducibility_ subsection.


## Initialise Specifying Variables
```bash
TCK_REPO_LOC="/path_to/tmp_tcks" # Whereever you would wish to place this, but must be absolute path
LHCB_METAINFO_LOC="/path_to/tmp_lhcb_metainfo" # Whereever you would wish to place this, but must be absolute path
STACK_DIRECTORY_LOC="." # Or whereever you would wish to place this
STACK="RTA/<date>" # Whatever stack the relevant release is, even for a 'patched' production it's necessary information.
SETTINGS="<sequence to use>"
TYPE=$SETTINGS # Branch to use, for now just set it equal to settings.
TCK_ID="<next unused TCK ID>" # `git -C $TCK_REPO_LOC tags` will provide all TCKs
LABEL="<Useful information>" # i.e. description of what's changed with respect to last TCK
git clone https://:@gitlab.cern.ch:8443/lhcb/tcks.git $TCK_REPO_LOC
git clone https://:@gitlab.cern.ch:8443/lhcb-conddb/file-content-metadata.git $LHCB_METAINFO_LOC
```
Note Well:
Due to the nature of create_hlt1_tck, it will always clone the LHCbFileContentMetaDataRepo into the local directory when being used.
We want to use that cloned+edited repo for pushing appropriate metadata, not the original repo.

## Generate the TCK
Use a stack including the changes intended for the TCK.
'Patches', referred to later in _Reproducibility_, are the commits of any projects (Allen/its dependencies) that are not on a release version.
The TCK will be written to `$TCK_REPO_LOC` and the appropriate metadata will be in `$LHCB_METAINFO_LOC`
```bash
${STACK_DIRECTORY_LOC}/Allen/run env LHCbFileContentMetaDataRepo=$LHCB_METAINFO_LOC python ${STACK_DIRECTORY_LOC}/Allen/Rec/Allen/scripts/create_hlt1_tck.py $STACK $SETTINGS ${TCK_REPO_LOC}/.git $TCK_ID --label "$LABEL" -t $TYPE
```


## For local testing
To run the testbench to evaluate TCKs, it is necessary to be run these instructions on a GPU machine, e.g. `ssh hlt_oper@n4050501`.
To run with the newly generated TCKs, it is also necessary to point MooreOnline's environment to your local test output like so:
```bash
DATA_DIR='<path to data meps, E.g. /calib/online/MEP_dumps_23_08_24/, should be recent>' # If you are not sure, ask the experts for appropriate data to validate the new TCK performance with.
${STACK_DIRECTORY_LOC}/MooreOnline/run env TCK_REPO=${TCK_REPO_LOC}/.git LHCbFileContentMetaDataRepo=${LHCB_METAINFO_LOC}/.git ${STACK_DIRECTORY_LOC}/MooreOnline/MooreScripts/scripts/testbench.py --hlt-type=${TCK_REPO_LOC}/.git:${TCK_ID} --working-dir=/tmp/hlt1_new_tck/ --measure-throughput=120 ${STACK_DIRECTORY_LOC}/MooreOnline/MooreScripts/tests/options/HLT1/Arch.xml --data-dir=$DATA_DIR
```

## For Trigger configuration comparisons
```bash
REFERENCE_TCK_ID="0x1..."
TCK_ID_TO_TEST="0x1..."
git -C $TCK_REPO_LOC diff ${REFERENCE_TCK_ID} ${TCK_ID_TO_TEST} 2>&1 | tee comparison_${REFERENCE_TCK_ID}_${TCK_ID_TO_TEST}.diff 
```


## Once experts are happy with the changes
#### Reproducibility
Create a MR in this repository (https://gitlab.cern.ch/lhcb/tck-specs) for these TCKs (even if the pipelines will not be working); Place the comparison diffs and the specifying variables as used above in the MR for posterity.

All patches, i.e. commits included in Allen + its dependencies that are not a released version, must be tagged.
As an example of an automated commit see: https://gitlab.cern.ch/lhcb/Allen/-/tags/tag__0x1000106f
```bash
git -C ${STACK_DIRECTORY_LOC}/Allen tag -a tag__0x10001075 4cc9dd401b4fe4f6700018de3cd1bcbc69b162a6 -m "Automatically Tagged by TCK Specification Repository CI_PIPELINE_ID=8004301."
git -C ${STACK_DIRECTORY_LOC}/Allen push origin tag <tag_name>
```

#### Deploy new TCK
Push the newly created tck tag (should be called ${tck_id}) to tck_repo_loc. 
```bash
git -C $TCK_REPO_LOC push origin $TCK_ID -u 
```
Deploy the new tck-specs tag to cvmfs by refreshing any 'deploy-cvmfs' successful job in tck-specs's main pipeline history. 
For Example: https://gitlab.cern.ch/lhcb/tck-specs/-/jobs/42771876

#### metadata (encoding keys) might exist and thus need to be pushed.
Note Well:
Due to the nature of create_hlt1_tck, it will always clone the LHCbFileContentMetaDataRepo into the local directory when being used.
We want to use that cloned+edited repo for pushing appropriate metadata, not the original repo.

```bash
git -C $LHCB_METAINFO_LOC diff --names-only origin/master
git -C $LHCB_METAINFO_LOC checkout -b $BRANCH_NAME # Something useful/descriptive etc.
```
`git cherry-pick`` the automated commits for the relevant tags that need pushing. 
Then push the branch and open a new Merge Request to metainfo's master.
```bash
git -C $LHCB_METAINFO_LOC push origin $BRANCH_NAME -u
```

For using the TCK(s) in data taking now, continue from step 7. (make TCKs known to cvmfs) in the Piquet Guide  