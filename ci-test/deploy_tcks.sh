#!/usr/bin/env bash
set -euo pipefail

echo "Deploying tcks.git to cvmfs"
task_uuid=$(curl -L -X PUT \
    -H "Authorization: Bearer $REQUEST_DEPLOY_TCKS_TOKEN" \
    https://lhcb-core-tasks.web.cern.ch/hooks/tcks/update-repo/ | tr -d '"')

echo "Sucessfully sent task with ID ${task_uuid}"

while true; do
    sleep 1;
    current_status=$(curl --silent -L "https://lhcb-core-tasks.web.cern.ch/tasks/status/${task_uuid}" | tr -d '"');
    echo "$(date -u): Status is ${current_status}";
    if [[ "${current_status}" == "SUCCESS" ]]; then
        return 0;
    elif [[ "${current_status}" == "FAILURE" ]]; then
        return 1;
    else
        sleep 30;
    fi
done
