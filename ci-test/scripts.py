import argparse
import json
import re
from stages import extract, check, generate, compare, on_merge, test
from typing import Callable


def hackily_change_dataPackages():
    """
        We don't want the possiblity of generating a TCK based on ParamFiles or some such.
        Hence we fix the dataPackages to just be PRConfig
    """
    fpath = 'utils/config.json'
    print('hackily fixing config.dataPackages=["DBASE/PRConfig"]')
    with open(fpath, 'r') as f:
        config = json.load(f)
        config['dataPackages'] = ['DBASE/PRConfig']
    print(config)
    with open(fpath, 'w') as f:
        json.dump(config, f, indent=4)
    return 0


def hackily_change_hlt1sequence(tck_id: str, tck_sequence: str):
    ret = 0

    fpath = 'Moore/Hlt/Hlt1Conf/tests/options/bandwidth/hlt1_bandwidth_streamless_streams.py'
    new_line = f'allen_gaudi_config.global_bind(sequence="{tck_sequence}")'
    pattern = r'sequence='

    print(f'hackily changing hlt1_sequence in BW Tests for tck \'{tck_id}\'')
    with open(fpath, 'r') as f:
        content = f.readlines()
        success = False
        for i, line in enumerate(content):
            print(f"{i}: {line}")
            if re.search(pattern, line):
                content.insert(i - 3, new_line)
                success = True
        if not success:
            print(f'ERROR: Failed to find {pattern} in {fpath} for {tck_id}')
            ret += 1
    with open(fpath, 'w') as f:
        f.writelines(content)

    return ret


# Argparse
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description=__doc__)
    subparsers = parser.add_subparsers(help='Action to run')

    def create_subparser(func: Callable):
        """
            Initiliases, for a given function, a subparser within the subparsers object.
            Inputs:
                - function to generate subparser for
            Outputs:
                - initialised subparser for the given function
        """
        sp = subparsers.add_parser(
            func.__name__.replace('_', '-'), description=func.__doc__)
        sp.set_defaults(_function=func)
        return sp

    extract_TCK_specs_parser = create_subparser(extract.specs)
    extract_TCK_specs_parser.add_argument(
        '--all-specs',
        nargs='+',
        type=str,
        required=True,
        help='List of all tck specifications on current branch')
    extract_TCK_specs_parser.add_argument(
        '--tags',
        nargs='+',
        type=str,
        required=True,
        help='List of all tags in tck_repo')

    check_comparison_parser = create_subparser(check.check_comparison)
    tck_id_parser = create_subparser(check.id)
    for p in [tck_id_parser, check_comparison_parser]:
        p.add_argument(
            '--tck-tags',
            nargs='+',
            type=str,
            required=True,
            help='list of tcks to tck repository tags for comparison')

    tck_push_parser = create_subparser(on_merge.tck_push)
    tck_push_parser.add_argument(
        '--tck-https',
        type=str,
        required=True,
        help='Location of remote tck specification repository')

    project_push_parser = create_subparser(on_merge.project_tag)
    for project in ["allen", "lbcom", "lhcb", "moore", "rec"]:
        project_push_parser.add_argument(
            f'--{project}-token',
            type=str,
            nargs='?',
            default='',
            help='developer CI tokens to allow pushing a tag to a stack project'
        )
    project_push_parser.add_argument(
        '--pipeline-id',
        type=str,
        nargs='?',
        default='',
        help='CI_PIPELINE_ID variable, for more verbose tag message')

    testbench_parser = create_subparser(test.testbench)
    testbench_parser.add_argument(
        '--tck-specs-repo-id',
        type=str,
        required=True,
        help='ID for gitlab repository, needed for local testing message')
    testbench_parser.add_argument(
        '--generate-job-id',
        type=str,
        required=True,
        help='ID for tck generation job, needed for local testing message')

    workflow_and_settings_parser = create_subparser(
        check.workflow_and_settings)
    tck_generation_parser = create_subparser(generate.generate)
    tck_comparison_parser = create_subparser(compare.compare)
    for p in [
            tck_id_parser, workflow_and_settings_parser, tck_generation_parser,
            tck_comparison_parser, tck_push_parser, project_push_parser,
            testbench_parser, check_comparison_parser
    ]:
        p.add_argument(
            '--specs',
            nargs='+',
            type=str,
            required=True,
            help='list of filepaths to tck specification yamls')

    for p in [project_push_parser, tck_push_parser]:
        p.add_argument(
            '--dry-run',
            action='store_true',
            help='Flag to skip pushing tags.')

    hackily_change_dataPackages_parser = create_subparser(
        hackily_change_dataPackages)

    hackily_change_hlt1sequence_parser = create_subparser(
        hackily_change_hlt1sequence)
    hackily_change_hlt1sequence_parser.add_argument(
        '--tck-id', type=str, required=True)
    hackily_change_hlt1sequence_parser.add_argument(
        '--tck-sequence', type=str, required=True)

    # parse the arguments, extract the function and execute it
    args = parser.parse_args()
    conf = vars(args)
    function = conf.pop('_function')
    function(**conf)
