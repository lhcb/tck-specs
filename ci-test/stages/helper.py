import yaml
import subprocess
from enum import Enum

# Clarity definitions and helper functions
STACK_PREFIX = "https://gitlab.cern.ch/lhcb-core/lhcbstacks/-/raw/master/data/stacks"
ALLEN_SEQUENCE_HTML_TEMPLATE = "https://gitlab.cern.ch/lhcb/Allen/-/raw/{}/configuration/python/AllenSequences"
tmp_stack_loc = 'tmp/stack'
tck_repo_loc = 'tmp/tck_repo'
allen_repo_loc = tmp_stack_loc + '/Allen'

COMMIT_BASED_WORKFLOW = "commit-based"
STACK_BASED_WORKFLOW = "stack-based"
ALLEN_DEPENDENCIES = ['Rec', 'LbCom', 'LHCb', 'Detector', 'Gaudi']


def _urlopen(fpath: str):
    from urllib.request import urlopen
    from urllib.error import URLError, HTTPError
    msg = f"ERROR while accessing {fpath}:\n  {{}}"
    try:
        return urlopen(fpath)
    except HTTPError as e:
        raise RuntimeError(msg.format(e))
    except URLError as e:
        raise RuntimeError(msg.format(e))


class Workflow(str, Enum):
    STACK_BASED = 'stack-based'
    COMMIT_BASED = 'commit-based'


class Spec():
    def __init__(self, yaml_fpath: str, is_local: bool = True):
        d = self._get_dict(yaml_fpath=yaml_fpath, is_local=is_local)
        ps = d['parameters']
        self._key_checks(d=d, ps=ps)

        self.id = hex(d['TCK'])
        self.workflow = Workflow(d['workflow'])
        self.process = ps['process']
        self.type = ps['type']
        self.stack = ps['stack']
        self.settings = ps['settings']
        self.label = ps['label']
        self.commits = ps.get('commits', {})
        self.comparison = hex(ps['comparison']) if 'comparison' in ps else None

    def _key_checks(self, d: dict, ps: dict):
        d_required_keys = set(("workflow", "parameters", "TCK"))
        d_keys = set(d.keys())
        ps_required_keys = set(("process", "type", "stack", "settings",
                                "label"))
        ps_optional_keys = set(("commits", "comparison"))
        ps_keys = set(ps.keys())

        missing_d_keys = d_required_keys - d_keys
        if missing_d_keys:
            raise RuntimeError(f"Missing keys in dictionary: {missing_d_keys}")

        extra_d_keys = d_keys - d_required_keys
        if extra_d_keys:
            raise RuntimeError(f"Forbidden keys in dictionary: {extra_d_keys}")

        missing_ps_keys = ps_required_keys - ps_keys
        if missing_d_keys:
            raise RuntimeError(f"Missing parameters: {missing_ps_keys}")

        extra_ps_keys = ps_keys - ps_required_keys
        if extra_ps_keys:
            forbidden_ps_keys = extra_ps_keys - ps_optional_keys
            if forbidden_ps_keys:
                raise RuntimeError(
                    f"Found forbidden 'parameters' keys: {forbidden_ps_keys}")

        c = ps.get('commits', {})
        if c:
            allowed_projects = set(ALLEN_DEPENDENCIES + ['Allen', 'Moore'])
            projects = set(c.keys())
            forbidden_projects = projects - allowed_projects
            if forbidden_projects:
                raise RuntimeError(
                    f"Found forbidden projects in 'commits', {forbidden_projects}, must be a subset of {allowed_projects}"
                )

    def _get_dict(self, yaml_fpath: str, is_local: bool):
        if is_local:
            with open(yaml_fpath, "r") as f:
                if not f:
                    raise RuntimeError(
                        f"yaml [{yaml_fpath}] not found. Check specification.")
                return yaml.safe_load(f)
        else:
            with _urlopen(yaml_fpath) as f:
                return yaml.safe_load(f.read())

    def compatibility(self, other):
        """
            Returns 0 if compatible, 1 if incompatible
        """
        return not ((self.stack == other.stack) and
                    (self.workflow == other.workflow) and
                    (self.commits == other.commits))


def get_version(application: str, stack: str) -> str:
    """
        Accesses the RTA stack defined in a dict(tck_spec) to return the application's version number
        Inputs:
            - Which application (Project) to return.
            - Dictionary of tck_specification to investigate
        Outputs:
            - str: application version
    """
    with _urlopen(f"{STACK_PREFIX}/{stack}.yml") as f:
        return yaml.safe_load(f)['projects'][application]


def add_cvmfs_dependencies_and_stack_commits(
        project_dict: dict, from_commit_dict: dict, tck_id: str):
    """
        Returns a list of commands to utils/config.py which determines:
            - the appropriate projects to access from cvmfs
            - the appropriate projects to build from a commit
        Checks that commits are appropriate for tck:
            - i.e. differ only by python files to their cvmfs ancestors.
    """
    for p, c in from_commit_dict.items():
        if p in project_dict.keys():
            diff_cmd = f'git -C {tmp_stack_loc}/{p} diff {project_dict[p]} {c} --name-only'
            print(f'executing_command: {diff_cmd}')
            diff = subprocess.run(
                diff_cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
                shell=True)
            diff_fnames = diff.stdout.split('\n')
            non_py_files = [
                fname for fname in diff_fnames
                if (fname and not fname.endswith('.py'))
            ]
            if non_py_files:
                raise RuntimeError(
                    f"ERROR: Check Configuration. project {p}: commit {c} differs from version {project_dict[p]} non-python files:\n{non_py_files}"
                )
    from_cvmfs_dict = {  # project, version
        p: v
        for p, v in project_dict.items()
        if (p not in from_commit_dict.keys() and p in ALLEN_DEPENDENCIES)
    }
    for p in ['Allen', 'Moore']:
        if 'Allen' not in from_commit_dict.keys():
            from_commit_dict['Allen'] = project_dict['Allen']
        if 'Moore' not in from_commit_dict.keys():
            from_commit_dict['Moore'] = project_dict['Moore']

    print(f'setting cvmfs project versions according to:\n{from_cvmfs_dict}')
    print(
        f'setting projects based on commits according to:\n{from_commit_dict}')
    return [
        *[
            f'utils/config.py cvmfsProjects.{p} {v}'
            for p, v in from_cvmfs_dict.items()
        ], *[
            f'utils/config.py gitBranch.{p} {c}'
            for p, c in from_commit_dict.items()
        ]
    ]


def hackily_change_dataPackages_cmd():
    return 'python ../../ci-test/scripts.py hackily-change-dataPackages'


def hackily_change_hlt1sequence_cmd(tck_id: str, tck_sequence: str):
    return f'python ../../ci-test/scripts.py hackily-change-hlt1sequence --tck-id {tck_id} --tck-sequence {tck_sequence}'


def log_friendly_message(msg: str, messages: list[str]):
    print(msg)
    messages.append(msg)
    return 0


def decompose_tag(tag: str) -> tuple[str]:
    # tag = vXXrYYpZZ
    tmp_tag = tag
    version, release, patch = 0, 0, 0
    if len(tmp_tag.split('p')) > 1:
        tmp_tag, patch = tmp_tag.split('p')
    if len(tmp_tag.split('r')) > 1:
        tmp_tag, release = tmp_tag.split('r')
    if len(tmp_tag.split('v')) > 1:
        tmp_tag, version = tmp_tag.split('v')
    return (int(version), int(release), int(patch))


def extract_stack_dict(stack_name: str) -> str:
    with _urlopen(f"{STACK_PREFIX}/{stack_name}.yml") as f:
        return yaml.safe_load(f)


def extract_binary_tag_from_RTAStack(stack_dict: str) -> str:
    binary_tag = [
        tag for tag in stack_dict['platforms']
        if ('el9' in tag and 'opt' in tag and 'gcc' in tag and 'detdesc' in tag
            )
    ][0]
    return binary_tag


def build_new_stack_cmds(tck_spec: Spec) -> list:
    stack_dict = extract_stack_dict(stack_name=tck_spec.stack)
    lcg_v = stack_dict['toolchain']['version']
    binary_tag = extract_binary_tag_from_RTAStack(stack_dict=stack_dict)

    build_cmds = [
        f'mkdir -p {tmp_stack_loc}/.ccache',
        f'mkdir -p {tmp_stack_loc}__fake',
        f'mv {tmp_stack_loc}/.ccache {tmp_stack_loc}__fake/.ccache',
        f'rm -rf {tmp_stack_loc}',
        f'curl https://gitlab.cern.ch/rmatev/lb-stack-setup/raw/master/setup.py | python3 - {tmp_stack_loc}',
        f'mv {tmp_stack_loc}__fake/.ccache {tmp_stack_loc}/.ccache',
        f'cd {tmp_stack_loc}',
        f'utils/config.py lcgVersion {lcg_v}',
        f'utils/config.py binaryTag {binary_tag}',
        'utils/config.py useDistcc false',
        "utils/config.py -- cmakeFlags.Moore '-DLOKI_BUILD_FUNCTOR_CACHE=OFF'",
        *add_cvmfs_dependencies_and_stack_commits(
            tck_id=tck_spec.id,
            project_dict=stack_dict['projects'],
            from_commit_dict=tck_spec.commits),
        hackily_change_dataPackages_cmd(),
        "time make Moore BUILDFLAGS='-j6'",
    ]
    return build_cmds


def filecontentmetadata_env(inBaseDir: bool = False, inStackDir: bool = False):
    if inBaseDir:
        return 'env LHCbFileContentMetaDataRepo=tmp/lhcb-metainfo'
    if inStackDir:
        return 'env LHCbFileContentMetaDataRepo=../lhcb-metainfo'
    raise RuntimeError(
        "filecontentmetadata environment variable can't be determined")
