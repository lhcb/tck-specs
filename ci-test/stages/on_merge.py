import subprocess
from stages import helper
from stages.helper import Spec, Workflow


def tck_push(tck_https: str, specs: list[str], dry_run: bool):
    did_skip = []

    messages = []
    ret = 0
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)
    for yaml_fp in specs:
        tck_id = Spec(yaml_fp).id
        push_cmd = f'git -C {helper.tck_repo_loc} push origin {tck_id} -u'
        print(f"push_cmd:\n'{push_cmd}'\n")
        if dry_run:
            __log(f"WARNING:: Skipped pushing to TCK repo '{tck_https}'.")
            did_skip += [tck_id]
            continue

        push = subprocess.run(
            push_cmd,
            text=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            shell=True)
        print(push.stdout)
    print(
        "\n------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")

    if did_skip:
        raise RuntimeError(f"WARNING:: Skipped pushing TCKs for {did_skip}")
    if ret != 0:
        raise RuntimeError("Some pushing failed, please check logs.")
    print(f"SUCCESS:: Successfully pushed TCKs {specs}")
    print("FINISHED SUMMARY OF JOB\n")

    return ret


def project_tag(specs: list[str], pipeline_id: str, dry_run: bool,
                allen_token: str, lbcom_token: str, lhcb_token: str,
                moore_token: str, rec_token: str):
    tokens = {  # project : token
        "Allen": allen_token,
        "LbCom": lbcom_token,
        "LHCb": lhcb_token,
        "Moore": moore_token,
        "Rec": rec_token
    }
    token_origin_template = "https://oath2:{token}@gitlab.cern.ch/lhcb/{project}.git"

    did_skip = []
    messages = []
    ret = 0
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)

    # All tcks must share the same workflow, so can just evaluate the first one.
    first_tck_spec = Spec(specs[0])
    if first_tck_spec.workflow == Workflow.STACK_BASED:
        __log(
            f'Workflow is {Workflow.STACK_BASED}, skipping project tag and returning 0.'
        )
    elif first_tck_spec.workflow == Workflow.COMMIT_BASED:
        for yaml_fp in specs:
            tck_spec = Spec(yaml_fp)
            tck_id = tck_spec.id

            commit_dict = tck_spec.commits
            commit_message = f"Automatically Tagged by TCK Specification Repository CI_PIPELINE_ID={pipeline_id}.\n  Short Description: {tck_spec.label}"
            for project, commit in commit_dict.items():
                git_cmd_prefix = f'git -C {helper.tmp_stack_loc}/{project}'
                # We need to define the a new remote but via the token, so that we can still push. It's functionally the same as `origin` aside from that. Then push to that remote.
                token_remote_cmd = f'{git_cmd_prefix} config remote.token_origin.url > /dev/null || {git_cmd_prefix} remote add token_origin {token_origin_template.format(token=tokens[project], project=project)}'
                print(
                    f'Creating token_origin to allow pushing from CI via:\n    {token_remote_cmd}'
                )
                token_remote = subprocess.run(
                    token_remote_cmd,
                    text=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=True)
                print(token_remote.stdout)
                ret += token_remote.returncode
                if token_remote.returncode != 0:
                    __log(
                        f"ERROR:: while adding token_origin for (tck_id, project):\n    ({tck_id}, {project})"
                    )
                    did_skip += [tck_id]
                    continue

                tag = f"tag__{tck_id}"
                create_tag_cmd = f'{git_cmd_prefix} tag -a {tag} {commit} -m "{commit_message}"'
                print(f'Executing:\n{create_tag_cmd}\n')
                create_tag = subprocess.run(
                    create_tag_cmd,
                    text=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=True)
                print(create_tag.stdout)
                ret += create_tag.returncode
                __log(
                    f"Created tag '{tag}' in repository '{helper.tmp_stack_loc}/{project}', returned '{create_tag.returncode}'"
                )
                if create_tag.returncode != 0:
                    __log(
                        f"ERROR:: while creating tag '{tag}', check configuration."
                    )
                    did_skip += [tck_id]
                    continue

                push_cmd = f'{git_cmd_prefix} push token_origin {tag}'
                print(f"Executing:\n'{push_cmd}'\n")
                if dry_run:
                    __log(f"WARNING:: Skipping pushing {tag} to {project}")
                    did_skip += [tck_id]
                    continue

                push = subprocess.run(
                    push_cmd,
                    text=True,
                    stdout=subprocess.PIPE,
                    stderr=subprocess.STDOUT,
                    shell=True)
                print(push.stdout)
                ret += push.returncode
                __log(
                    f"Pushed tck tag '{tag}' to repository '{project}', returned '{push.returncode}'"
                )
                if push.returncode != 0:
                    __log(
                        f"ERROR:: while pushing tag '{tck_id}', check configuration."
                    )
                    did_skip += [tck_id]
                __log(' ')
    print("\nSTARTING SUMMARY OF JOB")
    print("------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")
    print("FINISHED SUMMARY OF JOB\n")

    if did_skip:
        raise RuntimeError(f"WARNING:: Skipped pushing TCK tags {did_skip}")
    if ret != 0:
        RuntimeError(
            "ERROR:: Failure found while pushing TCK tags. check logs")
    else:
        print(f"SUCCESS:: Successfully pushed TCKs tags {specs}")

    return ret
