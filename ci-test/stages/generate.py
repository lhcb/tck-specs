import subprocess
import os
from stages import helper
from stages.helper import Spec, Workflow
from typing import Callable


def migrate_metainfo_back_to_tmp(__log: Callable):
    """
        Due to the nature of create_hlt1_tck: it will always clone the LHCbFileContentMetaDataRepo into the local directory when being used.
        We want to persist that cloned+edited repo, not the original repo. 
        However the function call is in different directories depending on the workflow (commit-based vs stack-based)
        Thus here we forcibly overwrite the original repo with the cloned+edited repo. 
    """
    ret = 0
    loc = "{}/lhcb-metainfo"
    stack_loc = loc.format("tmp/stack")
    base_loc = loc.format(".")
    in_stack = os.path.isdir(stack_loc)
    in_base = os.path.isdir(base_loc)
    if in_stack and in_base:
        raise RuntimeError(
            "Found lhcb-metainfo clone in './' and 'tmp/stack/', this should not be possible"
        )
    if not (in_stack or in_base):
        raise RuntimeError(
            "Found no lhcb-metainfo clone to migrate. This should not be possible"
        )

    cmd_template = f"rm -rf {loc.format('tmp')}; mv {{}} {loc.format('tmp')}"

    def migrate(tmp_loc: str):
        retcode = 0
        cmd = cmd_template.format(tmp_loc)
        print(f"Executing {cmd}")
        sp = subprocess.run(
            cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            shell=True)
        if sp.returncode != 0:
            retcode += sp.returncode
            __log(
                f"ERROR:: Migrating lhcb-metainfo returned {sp.returncode}\n")
        return retcode

    if in_stack:
        ret += migrate(stack_loc)
    if in_base:
        ret += migrate(base_loc)

    return ret


def generate(specs: list[str]):
    """
        Generate, per tck_specification, the tck and tag it in tmp/tck_repo.git
        Inputs:
            - list of specifications to generate tcks for
        Outputs:
            - Returns sum(returncode of tck generation process)
            - tmp/tck_id.log = full stdout+stderr of tck generation process, per tck
    """

    messages = []
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)
    ret = 0

    # check::workflow_and_settings Allows us to do this only once.
    ref_tck_spec = Spec(specs[0])
    if ref_tck_spec.workflow == Workflow.STACK_BASED:
        generate_cmd_prefixes = [
            f"lb-run Allen/{helper.get_version('Allen', ref_tck_spec.stack)}",
            helper.filecontentmetadata_env(inBaseDir=True),
            "python '$ALLENROOT/scripts/create_hlt1_tck.py'"
        ]
        relative_repo_loc = helper.tck_repo_loc
    elif ref_tck_spec.workflow == Workflow.COMMIT_BASED:
        __log(
            'WARNING:: \'commit-based\' workflow => need to build Allen. This may take some time.'
        )

        build_cmds = helper.build_new_stack_cmds(ref_tck_spec)
        __log(f"\nBuilding stack via:\n{build_cmds}\n")
        build = subprocess.run(
            '; '.join(build_cmds),
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            shell=True)
        print(build.stdout)
        if build.returncode != 0:
            __log(
                f"ERROR:: Building stack finished with return code {build.returncode}\n"
            )
            ret += build.returncode
        else:
            __log(
                f"SUCCESS:: Building stack finished with return code {build.returncode}\n"
            )

        generate_cmd_prefixes = [
            f'cd {helper.tmp_stack_loc};', 'Allen/run',
            helper.filecontentmetadata_env(inStackDir=True),
            'python Allen/Rec/Allen/scripts/create_hlt1_tck.py'
        ]
        relative_repo_loc = helper.tck_repo_loc.replace('tmp', '..')
    for yaml_fp in specs:
        tck_spec = Spec(yaml_fp)
        tck_type = tck_spec.type
        tck_id = tck_spec.id

        # Checkout branch if it already exists, else create a new one.
        # This ensures 'history' of the tck type.
        checkout_cmd = f'git -C {helper.tck_repo_loc} checkout -f {tck_type}'
        __log(
            f"\nChecking out {tck_type} for specification: {yaml_fp} via:\n{checkout_cmd}\n"
        )
        checkout = subprocess.run(
            checkout_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True)
        print(checkout.stdout)
        if checkout.returncode != 0:
            __log(f"\nChecking out {tck_type} Failed, creating it instead.")
            create_cmd = f'git -C {helper.tck_repo_loc} checkout -b {tck_type}'
            create = subprocess.run(
                create_cmd,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
                shell=True)
            print(create.stdout)
            if create.returncode != 0:
                __log(
                    f"Failed to create {tck_type} branch, skipping generation for {tck_id}."
                )
                continue
        # Ordering is important for the variables: stack, sequence, repository, tck, --label, -t
        generating_variables = [
            f"{tck_spec.stack}", f"{tck_spec.settings}",
            f"{relative_repo_loc}/.git", f"{tck_id}",
            f"--label \"{tck_spec.label}\"", f"-t {tck_type}"
        ]

        # TCK job itself ends up being run from within tmp/stack/Allen, so tmp/tck_repo needs redirecting.
        generate_cmd = ' '.join(generate_cmd_prefixes + generating_variables)

        __log(
            f"\n\nGenerating tck for specification: {yaml_fp} via:\n{generate_cmd}\n"
        )
        generate = subprocess.run(
            generate_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            shell=True)
        print(generate.stdout)
        if generate.returncode != 0:
            __log(
                f"ERROR:: Generate tck {yaml_fp} finished with return code {generate.returncode}\n"
            )
            ret += generate.returncode
        else:
            __log(
                f"SUCCESS:: Generate tck {yaml_fp} finished with return code {generate.returncode}\n"
            )

        logfile = f'tmp/generate_{tck_id}.log'
        print(
            f"Creating {logfile} for tck generation from specification {yaml_fp}"
        )
        with open(logfile, 'w') as f:
            f.write(generate.stdout)
        __log(
            f"Created {logfile} for tck generation from specification {yaml_fp}"
        )
        __log('  ')

    ret += migrate_metainfo_back_to_tmp(__log=__log)

    # repeat the printing at the bottom of the log for ease of reading.
    print("\nSTARTING SUMMARY OF JOB")
    print("------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")
    if ret != 0:
        print("ERROR :: Some TCK generation failed, check logs")
    else:
        print("SUCCESS :: Generated TCKs")
    print("FINISHED SUMMARY OF JOB\n")

    # To get ci-job to return with the correct exit code
    with open('tmp/generation_ret_file.txt', 'w') as f:
        f.write(str(ret))
    return ret
