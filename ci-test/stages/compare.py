import subprocess
from stages import helper
from stages.helper import Spec


def compare(specs: list[str]):
    """
        "Generate diffs between tcks. Evaluates:
            - Evaluate diff 'tck_id' 'most recent tck_id on default branch'
            - If a new 'default' type tck is being defined:
                - diff that with the previous tck of that type

    """
    ret = 0
    messages = []
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)

    default_branch_name = {'Hlt1': 'hlt1_pp_default'}[Spec(specs[0]).process]
    default_branch_tags = _extract_recent_default_branch_tags(
        default_branch_name=default_branch_name)

    to_compare = {}
    Specs = [Spec(yaml_fp) for yaml_fp in specs]
    for spec in Specs:
        if spec.comparison:
            to_compare[spec.id] = spec.comparison
        else:
            to_compare[spec.id] = default_branch_tags[
                1] if spec.type == default_branch_name else default_branch_tags[
                    0]

    for id, comp in to_compare.items():
        ret += _compare_tcks(id, comp, logging=__log)

    # repeat the printing at the bottom of the log for ease of reading.
    print("\nSTARTING SUMMARY OF JOB")
    print("------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")
    if ret != 0:
        raise RuntimeError("ERROR :: Some TCK comparison failed, check logs")
    else:
        print("SUCCESS :: Generated TCKs")
    print("FINISHED SUMMARY OF JOB\n")

    return ret


def _extract_recent_default_branch_tags(default_branch_name: str):
    extract_recent_default_branch_tags = subprocess.run(
        f'git -C {helper.tck_repo_loc} checkout -f {default_branch_name}; git -C {helper.tck_repo_loc} tag --list --sort=-authordate --merged',
        text=True,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        shell=True)
    # Remove the '\n' at the end of the string.
    latest_tags_on_branch = [
        line for line in extract_recent_default_branch_tags.stdout.split('\n')
        if '0x1' in line
    ]
    print("latest_tags_on_branch:\n")
    print(latest_tags_on_branch)
    return latest_tags_on_branch


def _compare_tcks(new_tck_id, reference_tck_id, logging):
    print(
        f"Evaluating diff between reference tck {reference_tck_id} and new tck {new_tck_id}"
    )
    diff_cmd = f'git -C {helper.tck_repo_loc} diff --color {reference_tck_id} {new_tck_id}'
    print(f"Evaluating: {diff_cmd}")
    diff = subprocess.run(
        diff_cmd,
        stdout=subprocess.PIPE,
        stderr=subprocess.STDOUT,
        text=True,
        shell=True)
    print(diff.stdout)
    if diff.returncode != 0:
        logging(
            f"Error:: Returncode {diff.returncode} while creating diff between '{reference_tck_id}' and '{new_tck_id}'."
        )
    else:
        diff_file = f'tmp/{reference_tck_id}_{new_tck_id}.diff'
        with open(diff_file, 'w') as f:
            f.write(diff.stdout)
        logging(
            f"SUCCESS:: Created '{diff_file}' between the new tck, '{reference_tck_id}', and the reference tag, '{new_tck_id}"
        )
    return diff.returncode
