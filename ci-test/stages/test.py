from stages import helper
from stages.helper import Spec, Workflow


def testbench(specs: list[str], tck_specs_repo_id: str, generate_job_id: str):
    messages = []
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)
    ret = 0

    ref_tck_spec = Spec(specs[0])

    if ref_tck_spec.workflow == Workflow.COMMIT_BASED:
        make_MO_cmd = [f'cd {helper.tmp_stack_loc}', 'time make MooreOnline']
        __log(
            f"WARNING:: Skipped building MooreOnline with cmd:\n    {make_MO_cmd}"
        )

        #make_MO = subprocess.run(
        #    '; '.join(make_MO_cmd),
        #    text=True,
        #    stdout=subprocess.PIPE,
        #    stderr=subprocess.STDOUT,
        #    shell=True)

        #print(make_MO.stdout)
        #if make_MO.returncode != 0:
        #    __log(
        #        f"ERROR:: make MooreOnline returned {make_MO.returncode} with cmd:\n    {make_MO_cmd}"
        #    )
        #    ret += make_MO.returncode
        #placeholder_cmd_prefix = f"cd {helper.tmp_stack_loc}; MooreOnline/run"
    else:
        binary_tag = helper.extract_binary_tag_from_RTAStack(
            stack_dict=helper.extract_stack_dict(
                stack_name=ref_tck_spec.stack))
        placeholder_cmd_prefix = f"lb-run MooreOnline/latest -c {binary_tag}"
        __log(
            f"WARNING:: Skipped using CVMFS MooreOnline with cmd:\n    {placeholder_cmd_prefix}"
        )

    # repeat the printing at the bottom of the log for ease of reading.
    print("\nSTARTING SUMMARY OF JOB")
    print("------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")
    if ret != 0:
        raise RuntimeError(
            f"ERROR:: TestBench Tests returned {ret}, check logs")
    else:
        print("SUCCESS:: TestBench Tests passed.")
    print("FINISHED SUMMARY OF JOB\n")

    output_path = f"artifacts__{tck_specs_repo_id}__{generate_job_id}"
    metadata_loc = 'tmp/lhcb-metainfo'

    def _msg(msgs):
        for msg in msgs:
            print(f"    {msg}")

    print("\nINSTRUCTIONS TO ACCESS OUTPUT FROM PIPELINE:")
    _msg([
        f"curl --location --output {output_path}.zip https://gitlab.cern.ch/api/v4/projects/{tck_specs_repo_id}/jobs/{generate_job_id}/artifacts",
        f"unzip -q {output_path}.zip '{helper.tck_repo_loc}/*' '{metadata_loc}/*' -d {output_path}",
        f"LOCAL_TCK_REPO=$(pwd)/{output_path}/{helper.tck_repo_loc}/.git"
    ])
    print("FINISHED INSTRUCTIONS TO ACCESS OUTPUT FROM PIPELINE\n")

    print("\nINSTRUCTIONS TO COMPARE TCKs:")
    _msg([
        "<FOLLOW INSTRUCTIONS TO ACCESS OUTPUT FROM PIPELINE>",
        "REFERENCE_TCK_ID='0x1...'", "COMPARISON_TCK_ID='0x1...'",
        "git -C ${LOCAL_TCK_REPO} ${REFERENCE_TCK_ID} ${COMPARISON_TCK_ID}"
    ])
    print("FINISHED INSTRUCTIONS TO COMPARE TCKs\n")

    print("\nINSTRUCTIONS TO RUN TEST LOCALLY:")
    _msg([
        "<FOLLOW INSTRUCTIONS TO ACCESS OUTPUT FROM PIPELINE>",
        f'LOCAL_FCM=$(pwd)/{output_path}/{metadata_loc}/.git',
        "TCKID='<to be filled in by user, dependent on which tck to test>'",
        "LOCAL_TCK=${LOCAL_TCK_REPO}:$TCKID",
        "DATA_DIR='<path to data meps, should be recent.>'",
        "MooreOnline/run env LHCbFileContentMetaDataRepo=$LOCAL_FCM MooreOnline/MooreScripts/scripts/testbench.py --hlt-type=$LOCAL_TCK --working-dir=/tmp/hlt1_new_tck/  --measure-throughput=120 MooreOnline/MooreScripts/tests/options/HLT1/Arch.xml --data-dir=$DATA_DIR"
    ])
    print("FINISHED INSTRUCTIONS TO RUN TEST LOCALLY\n")
    return ret
