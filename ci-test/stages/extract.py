def specs(tags: list[str], all_specs: list[str]):
    """
        Given a the existing tck_repo:tags and all current specifications, generates the list of new tcks being added.
        Input:
            - List[tck_repo:tags]
            - List[spec_repo:all_specifications]
        Outputs: tmp/TCK_specs.txt
    """
    new_tcks = [
        filename for filename in all_specs if filename[:-5] not in tags
    ]
    if new_tcks:
        print(f"Found new TCKs to be generated:\n{new_tcks}")
    ofile = 'tmp/TCK_SPECS.txt'
    with open(ofile, 'w') as f:
        filepaths = [f'tck/{filename}' for filename in new_tcks]
        f.write('\n'.join(filepaths))
    return 0
