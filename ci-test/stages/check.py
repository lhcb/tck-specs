import subprocess
from stages import helper
from stages.helper import Spec, Workflow


def id(specs: list[str], tck_tags: list[str]):
    """
        Verify, per yaml specification, that:
            - the ID matches the specification name.
            - the ID is a 32-bit hex beginning with 0x1
            - the ID is not already used in the tck repo
            then: If all are viable, verify that they would leave no gaps in ID if merged.
        Inputs:
            - list of specifications to investigate
            - list of tags (in hex) in tck_repo.git
    """
    potential_ids = []  # [id_in_hex]
    int_list = [int(hex_id, 16) for hex_id in tck_tags]
    int_list.sort()
    next_available_hex_id = hex(int_list[-1] + 1)

    # Verify that the tck ID matches name + are not in use
    for yaml_fp in specs:
        spec = Spec(yaml_fp)
        tck_id = spec.id
        potential_ids.append(tck_id)
        # <dirpath>/0x1hhhhhhh.yaml -> 0x1hhhhhhh
        tck_id_from_fp = yaml_fp.split('/')[-1].replace('.yaml', '')

        if tck_id != tck_id_from_fp:
            raise RuntimeError(
                f"{yaml_fp} TCK's ID, {tck_id}, does not match the ID derived from the specification filename {tck_id_from_fp}."
            )
        if (int(tck_id, 16) > 536870911 or int(tck_id, 16) < 268435457):
            # tck_id > 0x1fffffff || tck_id < 0x10000001
            raise RuntimeError(
                f"{yaml_fp} TCK's ID, {tck_id}, is not a 32-bit hex beginngin with 0x1."
            )
        if any(used_tck == tck_id for used_tck in tck_tags):
            raise RuntimeError(
                f"{yaml_fp} TCK's ID, {tck_id}, is already in use. The next available TCK_ID is '{next_available_hex_id}'."
            )

    # Verify that the viable TCKs would leave no gaps
    while (len(potential_ids) > 0):
        for tck_id in potential_ids:
            if tck_id == next_available_hex_id:
                print(
                    f"TCK {tck_id} is the next available ID {next_available_hex_id}. Iterating over remaining TCK IDs."
                )
                next_available_hex_id = hex(int(next_available_hex_id, 16) + 1)
                potential_ids.remove(tck_id)
                break
            else:
                continue
        if (not any(tck_id == next_available_hex_id
                    for tck_id in potential_ids)) and (len(potential_ids) > 0):
            raise RuntimeError(
                f"None of TCK IDs {potential_ids} equal the next available TCK ID {next_available_hex_id}. The TCK IDs need to be subsequent values."
            )

    return 0


def workflow_and_settings(specs: list[str]):
    """
        Checks the following for every specification:
            - workflow is one of allowed configurations
        Further, when commits.Allen exists:
            - The stack:Allen_v is an Ancestor of commit
            - diff between Allen_v and commit is ___only___ python files.
        Inputs:
            - list of specifications to evaluate.
    """
    ret = 0
    messages = []
    __log = lambda msg: helper.log_friendly_message(messages=messages, msg=msg)

    if len(specs) > 1:
        ref_yaml_fp = specs[0]
        ref_tck_spec = Spec(ref_yaml_fp)
        for yaml_fp in specs:
            tck_spec = Spec(yaml_fp)
            ret += ref_tck_spec.compatibility(Spec(yaml_fp))
        if ret > 0:
            raise RuntimeError(
                "FAILURE::TCK specifications within a single MR need to share the same project Stack/Workflow/Commits. Check configuration"
            )

    # Now that we know they share the same configuration, we only need to do this for the first specification.
    standalone_allen_loc = 'tmp/Allen'
    tck_spec = Spec(specs[0])
    settings = tck_spec.settings
    allen_v = helper.get_version('Allen', tck_spec.stack)

    if tck_spec.workflow == Workflow.STACK_BASED and tck_spec.commits:
        raise RuntimeError(
            "Stack based TCK is built entirely based on CVMFS releases, commit dictionary not allowed"
        )

    allen_commit = tck_spec.commits.get('Allen')
    if allen_commit:
        allen_html = helper.ALLEN_SEQUENCE_HTML_TEMPLATE.format(allen_commit)
        checkout_cmd = f'git -C {standalone_allen_loc} checkout {allen_commit}'
        print(f'executing: {checkout_cmd}')
        checkout = subprocess.run(
            f'git -C {standalone_allen_loc} checkout {allen_commit}',
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            text=True,
            shell=True)
        print(checkout.stdout)
        if checkout.returncode != 0:
            __log(
                f'ERROR:: Issue checking out commit: {allen_commit}. Check configuration.'
            )
            ret += checkout.returncode

        ancestor_check = subprocess.run(
            f'git -C {standalone_allen_loc} merge-base --is-ancestor {allen_v} {allen_commit}',
            shell=True)
        ret += ancestor_check.returncode
        if ancestor_check.returncode != 0:
            __log(
                f'ERROR:: {allen_v} is not an ancestor of {allen_commit}. This is not allowed. Check configuration of {yaml_fp}.'
            )
        else:
            __log(f'SUCCESS:: {allen_v} is an ancestor of {allen_commit}.')

        diff_cmd = f'git -C {standalone_allen_loc} diff {allen_v} {allen_commit} --name-only'
        print(f'executing_command: {diff_cmd}')
        diff = subprocess.run(
            diff_cmd,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            text=True,
            shell=True)
        diff_fnames = diff.stdout.split('\n')
        non_py_files = [
            fname for fname in diff_fnames
            if (fname and not fname.endswith('.py'))
        ]
        if non_py_files:
            print(
                f'\nBeginning of non-python files found between {allen_v} and {allen_commit}:'
            )
            print(non_py_files)
            print(
                f'    : End of non-python files found between {allen_v} and {allen_commit}\n'
            )
            __log(
                f'ERROR:: Found changes to non-python files between {allen_v} and {allen_commit}. Must NOT be allowed!. Check configuration or release new stack.'
            )
            ret += 1
        else:
            __log(
                f'SUCCESS:: Verified that only difference between {allen_v} and {allen_commit} are python files. Suitable for use.'
            )
    else:
        print('\nAllen version is defined by a released stack')
        allen_html = helper.ALLEN_SEQUENCE_HTML_TEMPLATE.format(allen_v)

    settings_html = f'{allen_html}/{settings}.py'
    try:
        helper._urlopen(settings_html)
        __log(
            f'SUCCESS:: Verified that {settings_html} exists for configuration'
        )
    except:
        __log(
            f"ERROR:: settings {settings_html} doesn't exist. Check configuration."
        )

    # repeat the printing at the bottom of the log for ease of reading.
    print("\nSTARTING SUMMARY OF JOB")
    print("------------------------------------------------------------------")
    for msg in messages:
        print(msg)
    print("------------------------------------------------------------------")
    if ret != 0:
        print("ERROR :: workflow and settings are incompatible. Check Log")
    else:
        print("SUCCESS :: workflow and settings are compatible.")
    print("FINISHED SUMMARY OF JOB\n")
    return 0


def check_comparison(specs: list[str], tck_tags: list[str]):
    comparisons = {
        spec.id: spec.comparison
        for spec in [Spec(yaml_fp) for yaml_fp in specs]
    }
    err_msgs = []
    for id, comp in comparisons.items():
        if comp is None:
            print(
                f"No comparison provided for TCK {id}. Will default to heuristic instead."
            )
            continue

        if comp == id:
            err_msgs.append(f"TCK {id} cannot be compared to self")
            continue

        if comp in tck_tags:
            print(
                f"TCK {id}'s defined comparison TCK {comp} exists within known tck tags {tck_tags}."
            )
            continue

        if comp in list(comparisons.keys()):
            print(
                f"TCK {id}'s defined comparison TCK {comp} exists within 'to-be-generated' tcks {list(comparisons.keys())}."
            )
            continue

        err_msgs.append(
            f"TCK {id}'s defined comparison TCK {comp} does not exist within known tck tags {tck_tags} nor to-be-generated tcks {list(comparisons.keys())}"
        )

    if err_msgs:
        raise RuntimeError("\n  ".join(
            ["Found the following errors", *err_msgs]))
