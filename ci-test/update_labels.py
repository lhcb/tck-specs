#!/bin/env python
"""
Use the GitLab python API in order to update labels
of merge requests in the project.

Taken from https://gitlab.cern.ch/LHCb-QEE/ew/ew-analyses/-/blob/master/test/update-labels.py
"""
import argparse
import json
import logging
import subprocess

LOGGER = logging.getLogger()


def main(token, project_id, merge_request_iid, paths, labels):

    # convert the labels to a set to facilitate operations
    labels = set(labels)

    # path that allows to get a string with the data that we need in JSON format
    merge_request_url = f'https://gitlab.cern.ch/api/v4/projects/{project_id}/merge_requests/{merge_request_iid}'

    # we must be careful and avoid displaying the token on the screen!
    LOGGER.info(
        f'Authenticating to GitLab (project ID: {project_id}, merge request IID: {merge_request_iid})'
    )
    config = json.loads(
        subprocess.check_output(
            f'curl --silent --show-error --header "PRIVATE-TOKEN: {token}" "{merge_request_url}/changes"',
            shell=True))

    LOGGER.info('Scanning changes and looking for differences')
    try:
        new_labels = set(config['labels'])
    except:
        raise RuntimeError(config)
    if any((ch['old_path'].startswith(path) or ch['new_path'].startswith(path))
           for ch in config['changes'] for path in paths):
        LOGGER.info(f'Detected changes in paths {paths}')
        new_labels.update(labels)
    else:
        LOGGER.info(f'No changes detected in paths {paths}')
        new_labels.difference_update(labels)

    if len(new_labels) == len(config['labels']):
        LOGGER.info('Labels are already correct; nothing to add or remove')
    else:
        LOGGER.info('Updating merge request labels')
        # URL can not contain spaces and must be replaced by the plus sign
        sanitized_new_lables = ','.join(
            f'{s.replace(" ", "+")}' for s in new_labels)
        subprocess.check_output(
            f'curl --silent --show-error --header "PRIVATE-TOKEN: {token}" --request PUT --url {merge_request_url}?labels={sanitized_new_lables}',
            shell=True)


if __name__ == '__main__':

    logging.basicConfig(
        format='%(levelname)s: %(message)s', level=logging.INFO)

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        '--token',
        type=str,
        required=True,
        help='Token used for authentication with the GitLab server')
    parser.add_argument(
        '--project-id',
        type=int,
        default=180347,  # tck-specs
        help='Project identifier')
    parser.add_argument(
        '--merge-request-iid',
        type=int,
        required=True,
        help='IID of the merge request to process')
    parser.add_argument(
        '--paths',
        nargs='+',
        type=str,
        required=True,
        help=
        'Path in which we need to check for differences, considered with respect to the repository root directory'
    )
    parser.add_argument(
        '--labels',
        nargs='+',
        type=str,
        required=True,
        help='Label to add if changes are found')

    args = parser.parse_args()

    main(**vars(args))
